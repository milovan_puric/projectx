INCLUDE Irvine32.inc
INCLUDE macros.inc

BUFFER_SIZE = 5000

.data
buffer1 BYTE BUFFER_SIZE DUP(? )
buffer2 BYTE BUFFER_SIZE DUP(? )
bufferPatch BYTE BUFFER_SIZE DUP(? )
pocetak BYTE "PATCH", 0dh, 0ah
flag1 BYTE 00h
flag2 BYTE 00h
flag3 BYTE 00h
filename    BYTE 80 DUP(0)
fileHandle  HANDLE ?
end_line EQU <0dh, 0ah> ;kraj reda
endl BYTE end_line

.code
main PROC

; UCITAVANJE PRVOG FILE-a

; Unos imena 
mWrite "Unesite ime prvog file-a: "
mov	edx, OFFSET filename
mov	ecx, SIZEOF filename
call	ReadString

; Otvaranje file-a za unos
mov	edx, OFFSET filename
call	OpenInputFile
mov	fileHandle, eax

; Provera gresaka
cmp	eax, INVALID_HANDLE_VALUE ;greske pri otvaranju file-a
jne	file_ok1
mWrite <"Cannot open file", 0dh, 0ah>
jmp	quit
file_ok1 :

; Ucitavanje file-a u buffer
mov	edx, OFFSET buffer1
mov	ecx, BUFFER_SIZE
call	ReadFromFile
jnc	check_buffer_size1 ;error reading ?
mWrite "Error reading file. " ;yes: show error message
call	WriteWindowsMsg
jmp	close_file

check_buffer_size1 :
cmp	eax, BUFFER_SIZE ;provera da li je bafer dovoljne velicine 
jb	buf_size_ok1 
mWrite <"Error: Buffer too small for the file", 0dh, 0ah>
jmp	quit

buf_size_ok1:

; UCITAVANJE DRUGOG FILE-a

; Unos imena
mWrite "Unesite ime drugog file-a: "
mov	edx, OFFSET filename
mov	ecx, SIZEOF filename
call	ReadString

; Otvaranje file-a za unos
mov	edx, OFFSET filename
call	OpenInputFile
mov	fileHandle, eax

; Provera gresaka
cmp	eax, INVALID_HANDLE_VALUE ;greske pri otvaranju file-a
jne	file_ok2
mWrite <"Cannot open file", 0dh, 0ah>
jmp	quit
file_ok2 :

; Ucitavanje file-a u buffer
mov	edx, OFFSET buffer2
mov	ecx, BUFFER_SIZE
call	ReadFromFile
jnc	check_buffer_size2 ;error reading ?
mWrite "Error reading file. " ;yes: show error message
call	WriteWindowsMsg
jmp	close_file

check_buffer_size2 :
cmp	eax, BUFFER_SIZE ;provera da li je bafer dovoljne velicine
jb	buf_size_ok2
mWrite <"Error: Buffer too small for the file", 0dh, 0ah>
jmp	quit

buf_size_ok2:

; Dodavanje "PATCH" na pocetak file-a
mov ecx, (LENGTHOF pocetak)
mov esi, OFFSET pocetak
mov edi, OFFSET bufferPatch
cld
rep movsb

; Obrada

mov ecx, 0 ;brojac za bafer1
mov edx , 0 ;brojac za bafer2
mov edi, 7 ;brojac za baferPatch

mov al, 01h ; da u prvom ulasku u petlju ne zavrsio sa obradom

obrada:

cmp al, 0h 
jne nije_kraj ;kraj prvog file-a
cmp bl, 0h ; i kraj drugog
je ispis ; zavrsena obrada

nije_kraj:
mov al, flag1 ;provera da li se u oba file-a doslo do kraja reda
and al, flag2
jz label1
inc ecx
inc edx
mov flag1, 00h
mov flag2, 00h
and flag3, 01h ;provera da li je postojala razlika u redu
jnz label4
mov bufferPatch[edi], '0'
inc edi

label4:
mov bufferPatch[edi], 0dh
inc edi
mov bufferPatch[edi], 0ah
inc edi
mov flag3, 00h

label1:
mov al, [buffer1 + ecx]
inc ecx
mov bl, [buffer2 + edx]
inc edx
cmp al, endl ;provera da li se doslo do kraja reda u prvom file-u
jne nije1 
dec ecx
mov flag1, 01h

nije1:
cmp bl, endl ;provera da li se doslo do kraja reda u drugom file-u
jne nije2
dec edx
mov flag2, 01h

nije2:
cmp al,bl ;poredjenje karakter po karakter
je obrada
cmp al, 0h
je label2
and flag1, 01h
jnz label2
cmp al, ' ' ;izbegavanje upisa ' ' na pocetku reda patch file-a
jne label5
and flag3, 01h
jz obrada

label5:
mov bufferPatch[edi], al
inc edi
mov flag3, 01h
jmp obrada

label2:
cmp bl, ' ' ;izbegavanje upisa ' ' na pocetku reda patch file-a
jne label6
and flag3, 01h
jz obrada

label6:
mov bufferPatch[edi], bl
inc edi
mov flag3, 01h ; oznaka da je nesto upisano u tom redu patch file-a
jmp obrada

ispis:

; UNOS U PATCH FILE

; Unos imena patch file-a
mWrite "Unesite ime PATCH file-a: "
mov	edx, OFFSET filename
mov	ecx, SIZEOF filename
call	ReadString

; Kreiranje izlaznog file-a
mov edx, OFFSET filename
call	CreateOutputFile
mov filehandle, eax

; Ispis u izlazni file
mov eax, fileHandle
mov edx, OFFSET bufferPatch
mov ecx, edi
call	WriteToFile
jnc close_file
mWrite "Error writing file. "; error message
call	WriteWindowsMsg
jmp	close_file

close_file:
mov	eax, fileHandle
call	CloseFile

quit :
invoke ExitProcess, 0
main ENDP

END main